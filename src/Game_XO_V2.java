import java.util.Scanner;

public class Game_XO_V2 {
	
	static int winner = 0, player=0, go, row, column;
	static char[][] board = {
		{'1','2','3'},
		{'4','5','6'},
		{'7','8','9'}};;
	
	public static void main(String[] args) {
		
		showWelcome();
			
			for(int i=0 ; i<9 && winner==0 ; i++) {
				showBoard();
				switchPlayer();
				input();
				if(checkWinner()==true) {
					showBoard();
					showWinner();
					break;
				}
		}
			if(checkWinner()==false) {
			showDraw();	
			}
	}
	
	
	static void showDraw() {
		if(winner == 0) 
			System.out.printf("\n\tHow boring, it is a draw\n");
	}

	static void showWinner() {
			System.out.printf("\n\"Congratuiations!!\", player %d [%c], YOU ARE THE WINNER!\n",winner,(winner==1)?'X':'O');
	}

	static void showWelcome() {
		System.out.printf("\t\tWelcome to Game XO");
	}
	
	static void showBoard() {
				System.out.println("\n");
				System.out.printf("\t\t %c | %c | %c\n",board[0][0],board[0][1],board[0][2]);
				System.out.print("\t\t---+---+---\n");
				System.out.printf("\t\t %c | %c | %c\n",board[1][0],board[1][1],board[1][2]);
				System.out.print("\t\t---+---+---\n");
				System.out.printf("\t\t %c | %c | %c\n",board[2][0],board[2][1],board[2][2]);	
	}
	
	static void switchPlayer() {
		if(player==2) {
			player -=1;
		}else {
			player+=1;
		}
		
	}
	
	static void input() {
		Scanner kb= new Scanner(System.in);
		do {
			System.out.println("");
			System.out.printf("\nPlayer %d,please enter the number [your] %c :",player,(player==1)?'X':'O');
			go = kb.nextInt();
			if(!(go>=1 && go<=9)){
				System.out.println();
				System.out.print("!!! ERROR !!! ,Please enter the number");
				System.out.printf("\nPlayer %d,please enter the number [your] %c :",player,(player==1)?'X':'O');
				go = kb.nextInt();
			}
			row = --go/3;
			column = go%3;
		}while (go<0 || go>9 || board[row][column] > '9');
	
		board[row][column] = (player == 1)?'X':'O';
	}
	
	static boolean checkWinner() {
		
		if( (board[0][0] == board [1][1] && board[0][0] == board [2][2]) ||
				board[0][2] == board [1][1] && board[0][2] == board [2][0]) {
			winner = player;
			return true;
		}else {
			for(int line=0 ; line <=2 ; line++) {
				if((board[line][0] == board[line][1] && board[line][0] == board[line][2])||
						(board[0][line] == board[1][line] && board[0][line] == board[2][line])) {
					winner = player;
					return true;
				}
			}
		}
		return false;
	}
}
