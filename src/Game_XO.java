import java.util.Scanner;
public class Game_XO {

	public static void main(String[] args) {
		Scanner kb= new Scanner(System.in);
		int winner = 0, player=0, go, row, column;
		char[][] board = new char[][] {
			{'1','2','3'},
			{'4','5','6'},
			{'7','8','9'}};

			for(int i=0 ; i<9 && winner==0 ; i++) {
				System.out.println("\n");
				System.out.printf("\t\t\t\t %c | %c | %c\n",board[0][0],board[0][1],board[0][2]);
				System.out.print("\t\t\t\t---+---+---\n");
				System.out.printf("\t\t\t\t %c | %c | %c\n",board[1][0],board[1][1],board[1][2]);
				System.out.print("\t\t\t\t---+---+---\n");
				System.out.printf("\t\t\t\t %c | %c | %c\n",board[2][0],board[2][1],board[2][2]);
				player = i%2 +1;
				
				
				do {
					System.out.println("");
					System.out.printf("\n\t\t Player %d,please enter the number [your] %c :",player,(player==1)?'X':'O');
					
					go = kb.nextInt();
					row = --go/3;
					column = go%3;
				}while (go<0 || go>9 || board[row][column] > '9');
				
				board[row][column] = (player == 1)?'X':'O';
				
				if( (board[0][0] == board [1][1] && board[0][0] == board [2][2]) ||
						board[0][2] == board [1][1] && board[0][2] == board [2][0]) {
					
					winner = player;
				}else {
					for(int line=0 ; line <=2 ; line++) {
						if((board[line][0] == board[line][1] && board[line][0] == board[line][2])||
								(board[0][line] == board[1][line] && board[0][line] == board[2][line])) {
							winner = player;
						}
					}
				}
				
			}
			System.out.println("\n");
			System.out.printf("\t\t\t\t %c | %c | %c\n",board[0][0],board[0][1],board[0][2]);
			System.out.print("\t\t\t\t---+---+---\n");
			System.out.printf("\t\t\t\t %c | %c | %c\n",board[1][0],board[1][1],board[1][2]);
			System.out.print("\t\t\t\t---+---+---\n");
			System.out.printf("\t\t\t\t %c | %c | %c\n",board[2][0],board[2][1],board[2][2]);
			
			if(winner == 0) {
				System.out.printf("\n\t\t\tHow boring, it is a draw\n");
			}else {
				System.out.printf("\n\t\t\"Congratuiations!!\", player %d [%c], YOU ARE THE WINNER!\n",winner,(winner==1)?'X':'O');
			}
	}

}
