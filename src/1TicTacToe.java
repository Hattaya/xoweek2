import java.util.* ;
public class TicTacToe {
	static Scanner in ;
	static String[] Board ;
	static String Turn ;
	static String Winner = null ;
 
	public static void main(String[] args) {
		Scanner kb = new Scanner (System.in) ;
		Board = new String [9] ;
		Turn = "X" ;
		PopulateEmptyBoard() ;
		System.out.println("\t\t\t\t\t " + "Hello! The winner is X or O?" + "\n") ;
		printBoard() ;
		
		
		while (Winner == null) {
			System.out.println("Mr." + Turn + "'s. Enter a slot number[1 to 9] to place " + Turn + " in : ") ;
			int N ;
			try {
				N = kb.nextInt() ;
				if(!(N > 0 && N <= 9)) {
					System.out.println("Invalid input.... Pls. re-enter slot number: ") ;
					continue ;
				}
			} 
			catch (InputMismatchException e) {
				System.out.println("Invalid input.... Pls. re-enter slot number: ") ;
				continue ;
			}
			if (Board[N - 1].equals(String.valueOf(N))) {
				Board[N - 1] = Turn ;
				if(Turn.equals("X")) {
					Turn = "O" ;
				}
				else
					Turn = "X" ;
				printBoard() ;
				checkWinner() ;
			}
			else {
				System.out.println("Slot already taken.... Pls. re-enter slot number : ") ;
				continue ;
			}
		}
		
	}
	static String checkWinner() {
		for(int a = 0 ; a < 9 ; a++) {
			if((Board[0] == Board[4] && Board[0] == Board[8]) || (Board[6] == Board[4] && Board[6] == Board[2])) {
				Winner = Board[4] ;
			}
			else if (a/3 == 0 && (a%3 == 0 || a%3 == 1 || a%3 == 2)) {
				Winner = Board[a] ;
			}
		}
		for(int a = 0 ; a < 9 ; a++) {
			if(Arrays.asList(Board).contains(String.valueOf(a + 1))) {
				break ;
			}
			else if (a == 8)
				return "draw" ;
		}
		return Winner ;
	}
	static void printBoard() {
		
		System.out.println("\t\t\t\t\t\t " + Board[0] + " | " + Board[1] + " | " + Board[2]);
		System.out.println("\t\t\t\t\t\t" + "-----------");
		System.out.println("\t\t\t\t\t\t " + Board[3] + " | " + Board[4] + " | " + Board[5]);
		System.out.println("\t\t\t\t\t\t" + "-----------");
		System.out.println("\t\t\t\t\t\t " + Board[6] + " | " + Board[7] + " | " + Board[8]);
	}
	static void PopulateEmptyBoard() {
		for (int a = 0 ; a < 9 ; a++) {
			Board[a] = String.valueOf(a + 1) ;
		}
	}

}
